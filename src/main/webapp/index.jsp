<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<div align="center">
    <h1 align="center"><%= "Elective" %></h1>

    <p>
        <h2>Login</h2>
    </p>
    <form action="${pageContext.request.contextPath}/login" method="post">
        <table>
            <tr>
                <td>E-mail</td>
                <td>
                    <label>
                    <input type="text" name="email" required minlength="6" maxlength="45">
                    </label>
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <label>
                    <input type="password" name="password" required minlength="8" maxlength="45">
                    </label>
                </td>
            </tr>
        </table>
        <p class="loginResult">${sessionScope.loginResult}</p>
        <p><input type="submit" value="Log in"></p>
    </form>
    <p>
        <a href="${pageContext.request.contextPath}/registration">Registration</a>
    </p>
</div>

</body>
</html>