package com.epam.finalproject.controller.servlets.login;

import com.epam.finalproject.controller.dao.UserDAO;
import com.epam.finalproject.model.elective.User;
import com.epam.finalproject.model.util.ConnectionBuilder;
import com.epam.finalproject.model.util.PoolConnectionBuilder;
import jakarta.servlet.ServletException;

import java.sql.Connection;

public class LoginDAO {

    private UserDAO userDAO = new UserDAO();

    public boolean validateLogin(LoginBean loginBean){
        userDAO.setConnection(new PoolConnectionBuilder());
        return userDAO.validateLogin(loginBean);
    }

    public User getUser(LoginBean loginBean){
        userDAO.setConnection(new PoolConnectionBuilder());
        return userDAO.getUser(loginBean);
    }
}
