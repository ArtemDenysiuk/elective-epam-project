package com.epam.finalproject.controller.servlets.registration;

import com.epam.finalproject.controller.dao.UserDAO;
import com.epam.finalproject.model.elective.User;

public class RegistrationDAO {

    private UserDAO userDAO = new UserDAO();

    public int registerUser(User user){
        return userDAO.registerUser(user);
    }
}
