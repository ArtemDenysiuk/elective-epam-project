package com.epam.finalproject.controller.servlets.registration;

import com.epam.finalproject.controller.dao.UserDAO;
import com.epam.finalproject.controller.servlets.login.LoginBean;
import com.epam.finalproject.controller.servlets.login.LoginDAO;
import com.epam.finalproject.model.elective.User;
import com.epam.finalproject.model.enums.AccountStatus;
import com.epam.finalproject.model.enums.Roles;
import com.epam.finalproject.model.util.PoolConnectionBuilder;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "registration", value = "/registration")
public class RegistrationServlet extends HttpServlet {

    private RegistrationDAO registrationDAO;

    private final LoginDAO loginDAO = new LoginDAO();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/user/registration.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        LoginBean loginBean = new LoginBean();
        loginBean.setEmail(req.getParameter("email"));
        loginBean.setPassword(req.getParameter("password"));

        if(loginDAO.validateLogin(loginBean)){
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        }

        User user = createUser(req);

        if (registrationDAO.registerUser(user) > 0) {
            String message = "User registered successfully.";
            session.setAttribute("success-message", message);
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        } else {
            String message = "User registration failed.";
            session.setAttribute("fail-message", message);
            req.getRequestDispatcher("/WEB-INF/views/user/registration.jsp").forward(req, resp);
        }
    }

    private User createUser(HttpServletRequest req) {
        User user = new User();
        user.setFirstName(req.getParameter("firstName"));
        user.setSurname(req.getParameter("surname"));
        user.setMiddleName(req.getParameter("middleName"));
        user.setGender(req.getParameter("gender"));
        user.setEmail(req.getParameter("email"));
        user.setPassword(req.getParameter("password"));
        user.setRole(Roles.STUDENT);
        user.setStatus(AccountStatus.ACTIVE);
        return user;
    }

    @Override
    public void destroy() {

    }
}
