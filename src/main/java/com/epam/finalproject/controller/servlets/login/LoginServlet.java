package com.epam.finalproject.controller.servlets.login;

import com.epam.finalproject.controller.dao.UserDAO;
import com.epam.finalproject.model.elective.User;
import com.epam.finalproject.model.enums.Roles;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private final LoginDAO loginDAO = new LoginDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        LoginBean loginBean = new LoginBean();
        loginBean.setEmail(email);
        loginBean.setPassword(password);

        if(loginDAO.validateLogin(loginBean)){
            User user = loginDAO.getUser(loginBean);
            Roles role = user.getRole();
            HttpSession session =req.getSession();
            session.setAttribute("userID", user.getId());
            if(role == Roles.STUDENT){
                if(!user.isBlocked()){
                    req.getRequestDispatcher("/WEB-INF/views/user/main.jsp").forward(req, resp);
                }else{
                    req.getRequestDispatcher("index.jsp").forward(req, resp);
                }
            }
        }else{
            req.getRequestDispatcher("/WEB-INF/views/user/registration.jsp").forward(req, resp);
        }


    }
}
