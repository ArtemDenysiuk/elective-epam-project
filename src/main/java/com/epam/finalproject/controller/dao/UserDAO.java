package com.epam.finalproject.controller.dao;


import com.epam.finalproject.controller.servlets.login.LoginBean;
import com.epam.finalproject.controller.servlets.login.LoginDAO;
import com.epam.finalproject.model.elective.User;
import com.epam.finalproject.model.enums.AccountStatus;
import com.epam.finalproject.model.enums.Roles;
import com.epam.finalproject.model.util.ConnectionBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {

    private static ConnectionBuilder connection;

    public static Connection getConnection() throws SQLException {
        return connection.getConnection();
    }

    public void setConnection(ConnectionBuilder connection) {
        this.connection = connection;
    }

    public int registerUser(User user) {
        int i = 0;
        String REGISTER_USER_SQL = "INSERT INTO user(userStatus, userRole, email, password, firstName, surname, middleName, gender) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = UserDAO.getConnection();
             PreparedStatement st = connection.prepareStatement(REGISTER_USER_SQL)) {
            int count = 1;
            st.setString(count++, user.getStatus().toString());
            st.setString(count++, user.getRole().toString());
            st.setString(count++, user.getEmail());
            st.setString(count++, user.getPassword());
            st.setString(count++, user.getFirstName());
            st.setString(count++, user.getSurname());
            st.setString(count++, user.getMiddleName());
            st.setString(count, user.getGender());

            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("registerUser method class UserDAO");
        }
        return i;
    }

    public boolean validateLogin(LoginBean loginBean) {
        boolean validationStatus = false;

        String VALIDATE_USER_SQL = "SELECT * FROM user WHERE email = ? AND password = ?";

        try (
             PreparedStatement st = connection.getConnection().prepareStatement(VALIDATE_USER_SQL)) {
            st.setString(1, loginBean.getEmail());
            st.setString(2, loginBean.getPassword());

            ResultSet rs = st.executeQuery();
            validationStatus = rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("validateLogin method class UserDAO");
        }
        return validationStatus;
    }

    public User getUser(LoginBean loginBean) {

        User user = null;

        String GET_USER_SQL = "SELECT * FROM user WHERE email = ? AND password = ?";

        try (
             PreparedStatement st = connection.getConnection().prepareStatement(GET_USER_SQL)) {
            st.setString(1, loginBean.getEmail());
            st.setString(2, loginBean.getPassword());

            ResultSet rs = st.executeQuery();
            rs.next();
            user = new User(rs.getInt("id"), AccountStatus.valueOf(rs.getString("userStatus")), Roles.valueOf(rs.getString("userRole")),
                    rs.getString("firstName"), rs.getString("surname"), rs.getString("middleName"), rs.getString("gender"));

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("validateLogin method class UserDAO");
        }
        return user;
    }
}
