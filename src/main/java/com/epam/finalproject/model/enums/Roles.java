package com.epam.finalproject.model.enums;

public enum Roles {
    STUDENT, TEACHER, ADMIN;
}
