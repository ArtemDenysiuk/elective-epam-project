package com.epam.finalproject.model.enums;

public enum CourseStatus {
    EXPECTED, STARTED, ENDED;
}
