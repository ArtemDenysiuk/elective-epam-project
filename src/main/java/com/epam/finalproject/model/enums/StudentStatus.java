package com.epam.finalproject.model.enums;

public enum StudentStatus {
    REGISTERED, UNREGISTERED;
}
