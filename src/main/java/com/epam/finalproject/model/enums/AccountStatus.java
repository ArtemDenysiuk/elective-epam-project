package com.epam.finalproject.model.enums;

public enum AccountStatus {
    BLOCKED, ACTIVE
}
