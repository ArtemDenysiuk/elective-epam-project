package com.epam.finalproject.model.util;

import com.epam.finalproject.model.enums.AccountStatus;
import com.epam.finalproject.model.enums.Roles;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;

public class PoolConnectionBuilder implements ConnectionBuilder{
    private PoolConnectionBuilder instance;

    public synchronized PoolConnectionBuilder getInstance(){
        if (instance == null)
            instance = new PoolConnectionBuilder();
        return instance;
    }
    private static DataSource dataSource;

    public PoolConnectionBuilder(){
        try {
            Context ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/elective");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection con = null;
        try {
            con = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

    public static void main(String[] args) throws SQLException {
        final String user = "root";
        final String password = "Panda75096523";
        final String url = "jdbc:mysql://localhost:3306/elective";

        String REGISTER_USER_SQL = "INSERT INTO user(userStatus, userRole, email, password, firstName, surname, middleName, gender) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        //Connection connect = new PoolConnectionBuilder().getConnection();
        final Connection connect = DriverManager.getConnection(url, user, password);

        try (connect;
             PreparedStatement st = connect.prepareStatement(REGISTER_USER_SQL)){
            st.setString(1, AccountStatus.ACTIVE.toString());
            st.setString(2, Roles.STUDENT.toString());
            st.setString(3, "artem@gmail.com");
            st.setString(4, "123qwerty");
            st.setString(5, "Artem");
            st.setString(6, "Denysiuk");
            st.setString(7, "Oleksandrovych");
            st.setString(8, "Man");
            st.executeUpdate();
//            statement.setInt(1, 1);
//            final ResultSet resultSet = statement.executeQuery();
//
//            if(resultSet.next()){
//                String byName = "username is : "  + resultSet.getString("username");
//                System.out.println(byName);
//            }
        } finally {
            connect.close();
        }
    }


}

