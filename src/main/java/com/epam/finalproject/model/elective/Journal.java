package com.epam.finalproject.model.elective;

import com.epam.finalproject.model.enums.Roles;
import com.epam.finalproject.model.enums.StudentStatus;

public class Journal {
    private User student;

    private StudentStatus studentStatus;
    private int mark;

    public User getStudent() {
        return student;
    }

    public StudentStatus getStudentStatus() {
        return studentStatus;
    }

    public int getMark() {
        return mark;
    }

    public void setNote(User student, int mark) {
        if (student.getRole() == Roles.STUDENT & mark > 0) {
            this.student = student;
            this.mark = mark;
        }else{
            throw new IllegalArgumentException("Error in method setNote");
        }
    }

    public void setStudentStatus(StudentStatus studentStatus) {
        this.studentStatus = studentStatus;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}

