package com.epam.finalproject.model.elective;


import com.epam.finalproject.model.enums.CourseStatus;

import java.time.LocalDate;

public class Course {
    private long id;

    private String courseTheme;
    private String courseName;
    private CourseStatus status;
    private User teacher;
    private LocalDate durationTill;
    private Journal journal;
    private int studentsCount;


    //----------------getters---------------

    public long getId() {
        return id;
    }

    public CourseStatus getStatus() {
        return status;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseTheme() {
        return courseTheme;
    }

    public User getTeacher() {
        return teacher;
    }

    public Journal getJournal() {
        return journal;
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public LocalDate getDurationTill() {
        return durationTill;
    }

    //----------------setters---------------

    public void setId(long id) {
        this.id = id;
    }

    public void setStatus(CourseStatus status) {
        this.status = status;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setCourseTheme(String courseTheme) {
        this.courseTheme = courseTheme;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public void setStudentsCount(int studentsCount) {
        this.studentsCount = studentsCount;
    }

    public void setDurationTill(LocalDate durationTill) {
        this.durationTill = durationTill;
    }

    //------------toString--------------


    @Override
    public String toString() {
        return "Course:" +
                "\nid=" + id +
                "\ncourseTheme='" + courseTheme + '\'' +
                "\ncourseName='" + courseName + '\'' +
                "\nstatus=" + status +
                "\nteacherId=" + teacher.getId() +
                "\ndurationTill=" + durationTill +
                "\njournal=" + journal +
                "\nstudentsCount=" + studentsCount;
    }
}

