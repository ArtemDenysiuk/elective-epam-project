package com.epam.finalproject.model.elective;

import com.epam.finalproject.model.enums.AccountStatus;
import com.epam.finalproject.model.enums.Roles;

import java.util.List;

public class User {
    private int id;
    private String firstName;
    private String surname;
    private String middleName;
    private String email;
    private String password;
    private AccountStatus status;
    private Roles role;
    private String gender;
    private List<Course> courses;

//    User() {
//        id = Utility.createRandomNumber(5);
//        status = AccountStatus.ACTIVE;
//        role = Roles.STUDENT;
//    }

    public User(int id, AccountStatus status, Roles role, String firstName,
                String surname, String middleName, String gender) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surname = surname;
        this.status = status;
        this.role = role;
        this.gender = gender;
    }

    public User() {
    }

    /* getters */
    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public Roles getRole() {
        return role;
    }

    public String getGender() {
        return gender;
    }

    public List<Course> getCourses() {
        return courses;
    }

    /* setters */

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public void addCourse(Course course) {
        courses.add(course);
    }

    public void block() {
        this.setStatus(AccountStatus.BLOCKED);
    }

    public void unblock() {
        this.setStatus(AccountStatus.ACTIVE);
    }

    public boolean isBlocked() {
        return status == AccountStatus.BLOCKED;
    }

    /* toString */

    @Override
    public String toString() {
        return "User:" +
                //"\nid=" + id +
                "\nstatus='" + status +
                "\nfirstName='" + firstName + '\'' +
                "\nmiddleName='" + middleName + '\'' +
                "\nlastName='" + surname + '\'' +
                "\nemail='" + email + '\'' +
                "\npassword='" + password + '\'';
    }
}

